#!/bin/bash


pkg=( net-tools acpi steam python-pywal )

for i in ${pkg[@]}; do

    echo "Installing $i"
    sudo pacman -S --noconfirm --needed $i
done
