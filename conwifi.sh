#Run this script as sudo

#1. Killing all wpa_supplicant  and dhcpcd processes

killall wpa_supplicant dhcpcd

#2. Enabling wpa_supplicant and connecting  to wifi through device "wlp2s0"

wpa_supplicant -B -i wlp2s0 -c <(wpa_passphrase 'SSID''Password')

#3. Getting IP address thorugh dhcpcd

dhcpcd wlp2s0